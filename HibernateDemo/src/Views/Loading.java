/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import dao.LoginDAO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import org.jvnet.substance.skin.SubstanceSaharaLookAndFeel;

public class Loading extends javax.swing.JFrame {

    public Loading() {
        try {
            UIManager.setLookAndFeel(new SubstanceSaharaLookAndFeel());
        } catch (Exception e) {
        }
        initComponents();
        Thread();
        initConnect();
    }
    int val;

    public void initConnect() {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginDAO().initConnect();
            }
        });
    }

    public void Thread() {
        new Thread(new Runnable() {
            public void run() {
                for (int i = 1; i <= 50; i++) {
                    val = i;
                    try {
                        SwingUtilities.invokeLater(new Runnable() {
                            public void run() {
                                jProgressBar1.setValue(val);
                            }
                        });
                        Thread.sleep(20);
                        jLabel2.setText("Loading.");
                        Thread.sleep(20);
                        jLabel2.setText("Loading...");
                        Thread.sleep(15);
                        jLabel2.setText("Loading......");

                    } catch (Exception e) {
                    }
                }
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new frmLogin().setVisible(true);
                    }
                });
                dispose();
            }
        }).start();

    }

    private void initComponents() {
        jPanel1 = new javax.swing.JPanel();
        jProgressBar1 = new javax.swing.JProgressBar();
        jProgressBar1.setMaximum(50);
        jProgressBar1.setMinimum(0);
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jProgressBar1.setBackground(new java.awt.Color(255, 255, 255));
        jProgressBar1.setFont(new java.awt.Font("Time New Roman", 1, 11)); // NOI18N
        jProgressBar1.setForeground(new java.awt.Color(204, 255, 0));
        jProgressBar1.setStringPainted(true);
        jPanel1.add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 280, 490, 30));
        jLabel2.setFont(new java.awt.Font("Time New Roman", 1, 14)); // NOI18N
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 284, 130, 20));
        BufferedImage img = null;
        try {
            File file = new File("Asserts/images/panel.jpg");
            if (file.exists()) {
                img = ImageIO.read(file);
                java.awt.Image dimg = img.getScaledInstance(900, 400, java.awt.Image.SCALE_SMOOTH);
                jLabel1.setIcon(new javax.swing.ImageIcon(dimg)); // NOI18N
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 850, 310));
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - 850) / 2, (screenSize.height - 310) / 2, 850, 310);
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Loading().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify                     
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;

    // End of variables declaration                   
}
