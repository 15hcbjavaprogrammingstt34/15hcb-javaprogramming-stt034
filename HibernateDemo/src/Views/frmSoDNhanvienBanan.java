/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import CustomJTable.CustomCellRenderer;
import CustomJTable.WrapLayout;
import CustomJTable.CustomMonAnRenderer;
import dao.GoimonDAO;
import dao.HoadonDAO;
import dao.MonanDAO;
import dao.NhanvienDAO;
import dao.NhanvienbananDAO;
import dao.TrangthaibananDAO;
import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Font;
import java.util.List;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;

import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import pojo.Goimon;
import pojo.Monan;
import pojo.Hoadon;
import pojo.Nhanvien;
import pojo.Nhanvienbanan;
import pojo.Trangthaibanan;

public class frmSoDNhanvienBanan extends javax.swing.JFrame {

    private JList<Monan> listMenu;
    private final DefaultListModel<Monan> modelMenu = new DefaultListModel<>();
    public JPanel pnLayout;
    public JFrame mainFrame;
    private final TrangthaibananDAO daoTrangThai = new TrangthaibananDAO();
    private List<Trangthaibanan> listTT;
    private List<Nhanvienbanan> listNVBA;
    private List<Nhanvien> listNV;
    private final NhanvienDAO daoNV = new NhanvienDAO();
    private final MonanDAO daoMonan = new MonanDAO();
    private final NhanvienbananDAO dao = new NhanvienbananDAO();
    public static String idTeBan = "";
    public static int idNV = 0;
    Nhanvien nhanVien = null;
    private static frmSoDNhanvienBanan instance;
    public int maNVBA;
    public String idBanan_Tenban = "";
    GoimonDAO daoGM = new GoimonDAO();
    double TongTien = 0;

    public static frmSoDNhanvienBanan getInstance() {
        return instance;
    }

    public frmSoDNhanvienBanan() {

        initComponents();
        initLoad();
    }

    public void initLoad() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadListTable();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                loadDanhSachThucDon();
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                createPanelTrangThai();
            }
        }).start();

        new Thread(new Runnable() {
            @Override
            public void run() {
                loadComboboxNhanvien();
            }
        }).start();
        loadLogo();
    }

    public void loadLogo() {

        try {
            BufferedImage img = ImageIO.read(new File("src\\icon\\logo.jpg"));
            java.awt.Image dimg = img.getScaledInstance(lblLogo.getWidth(), lblLogo.getHeight(), java.awt.Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(dimg);
            lblLogo.setIcon(imageIcon);
            lblLogo.repaint();
        } catch (IOException ex) {

        }

    }

    public void loadDanhSachThucDon() {
        panelThucdon.setLayout(new BorderLayout());
        listMenu = createListMenu();
        panelThucdon.add(new JScrollPane(listMenu),
                BorderLayout.CENTER);
    }

    private JList<Monan> createListMenu() {
        List<Monan> list = daoMonan.layDanhSachMonanSudung(1);
        for (Monan monan : list) {
            modelMenu.addElement(monan);
        }
        JList<Monan> listMn = new JList<>(modelMenu);
        listMn.setCellRenderer(new CustomMonAnRenderer());
        return listMn;
    }

    public void loadComboboxNhanvien() {
        listNV = daoNV.layDanhSachNhanvien();

        Nhanvien nvPos = null;
        DefaultComboBoxModel cbModelNV = new DefaultComboBoxModel();
        for (Nhanvien vt : listNV) {
            if (vt.getManv() == frmSoDNhanvienBanan.idNV) {
                nvPos = vt;
            }
            cbModelNV.addElement(vt);
        }
        cbbNhanVien.setModel(cbModelNV);
        if (nvPos != null) {
            cbbNhanVien.setSelectedItem(nvPos);
        }

    }

    public void loadListTable() {
        panelBA.removeAll();
        listNVBA = dao.layDanhSachNhanvienbanan();
        JButton button;
        Dimension d = new Dimension(140, 110);

        for (final Nhanvienbanan nv : listNVBA) {

            button = new JButton(nv.getBanan().getTenban());
            switch (nv.getTrangthaibanan().getId()) {
                case 1:// trong
                    button.setBackground(hex2Rgb("#80FF80"));// xanh non chuoi
                    break;
                case 2:// co khach
                    button.setBackground(hex2Rgb("#FFC080"));// dacam
                    break;
                case 3://dang doi mon
                    button.setBackground(hex2Rgb("#8E00CE"));// tim
                    break;
                case 4://mon da xong
                    button.setBackground(Color.yellow);// vang
                    break;
            }
            button.setOpaque(true);

            button.setPreferredSize(d);
            button.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    javax.swing.SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {

                            idBanan_Tenban = formatNumber(nv.getId()) + "-" + nv.getBanan().getTenban();
                            txtTenBan.setText(idBanan_Tenban);
                            txtSoluong.setText(nv.getSokhach() + "");
                            maNVBA = nv.getId();
                            nhanVien = nv.getNhanvien();
                            loadChitietGoimon(maNVBA);
                            cbbNhanVien.setSelectedItem(nhanVien);
                            txtThoigian.setText(formatDate(nv.getNgay()));
                            if (nv.getNhanvien() != null) {
                                loadChitietGoimon(maNVBA);

                                TongTien = daoGM.tinhTien(maNVBA);
                                txtThanhToan.setText(formatNumberDecimal(TongTien));
                                txtTongTien.setText(formatNumberDecimal(TongTien));
                            }
                        }
                    });

                }
            });
            panelBA.add(button);

        }
        panelBA.setLayout(new WrapLayout(5, 5, 10));
        repaint();
        revalidate();

    }

    public void loadChitietGoimon(int idnvba) {

        List<Goimon> listGM = daoGM.layDanhSachTheoIdNvba(idnvba);

        if (listGM != null) {
//            if (listGM.size() > 0) {
            int i = 1;
            DefaultTableModel model = new DefaultTableModel(null, new Object[]{"STT", "Tên món", "Số lượng", "Đơn giá", "Tình trạng"});
            for (Goimon g : listGM) {
                model.addRow(new Object[]{formatNumber(i), g.getMonan().getTenmon(), g.getSoluong(), g.getMonan().getDongia(), g.getTrangthaimonan().getTentt()});
                i++;
            }
            tbChitietGoiMon.setModel(model);
            tbChitietGoiMon.getColumnModel().getColumn(0).setMinWidth(45);
            tbChitietGoiMon.getColumnModel().getColumn(0).setMaxWidth(45);
            tbChitietGoiMon.getColumnModel().getColumn(0).setWidth(45);
            tbChitietGoiMon.setDefaultRenderer(Object.class, new CustomCellRenderer());
            tbChitietGoiMon.setShowGrid(false);
            tbChitietGoiMon.setShowHorizontalLines(false);
            tbChitietGoiMon.setShowVerticalLines(false);
            tbChitietGoiMon.setBackground(Color.WHITE);
            // }
        }
    }

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf(colorStr.substring(1, 3), 16),
                Integer.valueOf(colorStr.substring(3, 5), 16),
                Integer.valueOf(colorStr.substring(5, 7), 16));
    }

    public String formatNumber(int num) {
        NumberFormat formatter = new DecimalFormat("000");
        return formatter.format(num);
    }

    public String formatNumberDecimal(double num) {
        NumberFormat formatter = new DecimalFormat("###,###,###");
        return formatter.format(num);
    }

    public String formatDate(Date date) {
        DateFormat simpledateformat = new SimpleDateFormat("HH:mm dd-MM-yyyy");
        return simpledateformat.format(date);
    }

    private void createPanelTrangThai() {
        FlowLayout flowlayout = new FlowLayout(FlowLayout.LEADING);
        pntrangthai.setLayout(flowlayout);

        int i = 1;
        for (Trangthaibanan trangthai : daoTrangThai.layDanhSachBA()) {
            switch (i) {
                case 1:// trong
                    JLabel lb01 = new JLabel("        ");
                    lb01.setBackground(hex2Rgb("#80FF80"));// xanh non chuoi
                    lb01.setOpaque(true);
                    JLabel lb02 = new JLabel(trangthai.getTentt());
                    lb02.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
                    lb02.setOpaque(true);
                    pntrangthai.add(lb01);
                    pntrangthai.add(lb02);
                    break;
                case 2:// co khach
                    JLabel lb03 = new JLabel("        ");
                    lb03.setBackground(hex2Rgb("#FFC080"));// dacam
                    lb03.setOpaque(true);
                    JLabel lb04 = new JLabel(trangthai.getTentt());
                    lb04.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
                    lb04.setOpaque(true);
                    pntrangthai.add(lb03);
                    pntrangthai.add(lb04);
                    break;
                case 3://dang doi mon
                    JLabel lb05 = new JLabel("        ");
                    lb05.setBackground(hex2Rgb("#8E00CE"));// tim
                    lb05.setOpaque(true);
                    JLabel lb06 = new JLabel(trangthai.getTentt());
                    lb06.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
                    lb06.setOpaque(true);
                    pntrangthai.add(lb05);
                    pntrangthai.add(lb06);
                    break;
                case 4://mon da xong
                    JLabel lb07 = new JLabel("        ");
                    lb07.setBackground(Color.yellow);// vang
                    lb07.setOpaque(true);
                    JLabel lb08 = new JLabel(trangthai.getTentt());
                    lb08.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
                    lb08.setOpaque(true);

                    pntrangthai.add(lb07);
                    pntrangthai.add(lb08);
                    break;
            }
            i++;
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pntrangthai = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        panelBA = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtThoigian = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cbbNhanVien = new javax.swing.JComboBox<String>();
        jLabel4 = new javax.swing.JLabel();
        txtSoluong = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        txtTenBan = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbChitietGoiMon = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txtPhiDV = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtTongTien = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtKhachDua = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextField8 = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtGiamGia = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtThanhToan = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        txtVAT = new javax.swing.JTextField();
        spinnerGiamGiaPT = new javax.swing.JSpinner();
        spinnerVAT = new javax.swing.JSpinner();
        btnGoimon = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        lblLogo = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        panelThucdon = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setBounds(new java.awt.Rectangle(5, 5, 5, 5));
        setMaximumSize(new java.awt.Dimension(200, 200));
        setMinimumSize(new java.awt.Dimension(200, 200));

        pntrangthai.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout pntrangthaiLayout = new javax.swing.GroupLayout(pntrangthai);
        pntrangthai.setLayout(pntrangthaiLayout);
        pntrangthaiLayout.setHorizontalGroup(
            pntrangthaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 316, Short.MAX_VALUE)
        );
        pntrangthaiLayout.setVerticalGroup(
            pntrangthaiLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 38, Short.MAX_VALUE)
        );

        panelBA.setBackground(new java.awt.Color(255, 255, 255));
        panelBA.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panelBA.setAutoscrolls(true);
        panelBA.setMaximumSize(new java.awt.Dimension(150, 150));

        javax.swing.GroupLayout panelBALayout = new javax.swing.GroupLayout(panelBA);
        panelBA.setLayout(panelBALayout);
        panelBALayout.setHorizontalGroup(
            panelBALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 713, Short.MAX_VALUE)
        );
        panelBALayout.setVerticalGroup(
            panelBALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 594, Short.MAX_VALUE)
        );

        jScrollPane1.setViewportView(panelBA);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel1.setText("Thời gian:");

        txtThoigian.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtThoigian.setForeground(new java.awt.Color(0, 153, 0));
        txtThoigian.setText("11:25 - 22/11/2016");

        jLabel2.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel2.setText("Thông tin:");

        jLabel3.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel3.setText("Nhân viên:");

        cbbNhanVien.setFont(new java.awt.Font("Serif", 1, 13)); // NOI18N
        cbbNhanVien.setForeground(new java.awt.Color(0, 204, 51));
        cbbNhanVien.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        cbbNhanVien.setMaximumSize(new java.awt.Dimension(180, 23));
        cbbNhanVien.setMinimumSize(new java.awt.Dimension(180, 23));
        cbbNhanVien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbNhanVienActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel4.setText("Số lượng: ");

        txtSoluong.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtSoluong.setForeground(new java.awt.Color(0, 204, 51));
        txtSoluong.setText("15");

        txtTenBan.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtTenBan.setForeground(new java.awt.Color(0, 204, 51));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(6, 6, 6)
                                .addComponent(txtThoigian, javax.swing.GroupLayout.PREFERRED_SIZE, 164, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator6))
                        .addGap(30, 30, 30)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtTenBan))
                            .addComponent(jSeparator7)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbbNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(31, 31, 31)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(txtSoluong)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txtThoigian, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(txtTenBan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(cbbNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSoluong, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        tbChitietGoiMon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbChitietGoiMon.setMaximumSize(new java.awt.Dimension(291, 410));
        tbChitietGoiMon.setMinimumSize(new java.awt.Dimension(291, 410));
        tbChitietGoiMon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbChitietGoiMonMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbChitietGoiMon);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jLabel5.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel5.setText("Phí DV: ");

        txtPhiDV.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtPhiDV.setForeground(new java.awt.Color(0, 204, 204));
        txtPhiDV.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtPhiDV.setText("0");
        txtPhiDV.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPhiDVActionPerformed(evt);
            }
        });
        txtPhiDV.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPhiDVKeyPressed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel6.setText("Giảm giá:");

        jLabel7.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel7.setText("Tổng tiền:");

        txtTongTien.setEditable(false);
        txtTongTien.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtTongTien.setForeground(new java.awt.Color(0, 204, 204));
        txtTongTien.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtTongTien.setText("0");

        jLabel8.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        jLabel8.setText("Thanh toán:");

        jLabel9.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel9.setText("VAT:");

        jLabel10.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel10.setText("Khách đưa:");

        txtKhachDua.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtKhachDua.setForeground(new java.awt.Color(0, 204, 204));
        txtKhachDua.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtKhachDua.setText("0");

        jLabel11.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel11.setText("Trả lại:");

        jTextField8.setEditable(false);
        jTextField8.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        jTextField8.setForeground(new java.awt.Color(0, 204, 204));
        jTextField8.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        jTextField8.setText("0");

        jLabel12.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel12.setText("%");

        jLabel13.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel13.setText("%");

        txtGiamGia.setEditable(false);
        txtGiamGia.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtGiamGia.setForeground(new java.awt.Color(0, 204, 204));
        txtGiamGia.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtGiamGia.setText("0");

        jLabel14.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel14.setText("VNĐ");

        jLabel15.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel15.setText("VNĐ");

        jLabel16.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel16.setText("VNĐ");

        jLabel17.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel17.setText("VNĐ");

        jLabel18.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel18.setText("VNĐ");

        jLabel19.setFont(new java.awt.Font("SansSerif", 0, 13)); // NOI18N
        jLabel19.setText("VNĐ");

        txtThanhToan.setEditable(false);
        txtThanhToan.setFont(new java.awt.Font("SansSerif", 1, 16)); // NOI18N
        txtThanhToan.setForeground(new java.awt.Color(204, 0, 0));
        txtThanhToan.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtThanhToan.setText("0");

        jLabel20.setFont(new java.awt.Font("SansSerif", 1, 16)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(153, 0, 0));
        jLabel20.setText("VNĐ");

        txtVAT.setEditable(false);
        txtVAT.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        txtVAT.setForeground(new java.awt.Color(0, 204, 204));
        txtVAT.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtVAT.setText("0");

        spinnerGiamGiaPT.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        spinnerGiamGiaPT.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));
        spinnerGiamGiaPT.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerGiamGiaPTStateChanged(evt);
            }
        });

        spinnerVAT.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        spinnerVAT.setModel(new javax.swing.SpinnerNumberModel(0, 0, 100, 1));
        spinnerVAT.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                spinnerVATStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(31, 31, 31)
                                .addComponent(spinnerVAT, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtVAT, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator2)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGap(4, 4, 4)
                                        .addComponent(spinnerGiamGiaPT, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel12)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtGiamGia))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txtPhiDV, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel15)))
                            .addComponent(jSeparator3)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtKhachDua, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel17)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jSeparator4)
                            .addComponent(jLabel8)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(txtThanhToan, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel20))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(14, 14, 14)
                                .addComponent(txtTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel18))
                            .addComponent(jSeparator5)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jTextField8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel19)))
                        .addGap(17, 17, 17))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtPhiDV, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(txtTongTien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel14)
                    .addComponent(jLabel18))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jSeparator1)
                    .addComponent(jSeparator4))
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8)
                    .addComponent(jLabel12)
                    .addComponent(txtGiamGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(spinnerGiamGiaPT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtThanhToan, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel20)
                        .addComponent(jLabel13)
                        .addComponent(jLabel16)
                        .addComponent(txtVAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(spinnerVAT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel11)
                                .addComponent(jTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel19))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel10)
                                .addComponent(txtKhachDua, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel17))))
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        btnGoimon.setText("Gọi Món");
        btnGoimon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGoimonActionPerformed(evt);
            }
        });

        jButton3.setText("jButton3");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("jButton4");

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        lblLogo.setBackground(new java.awt.Color(255, 255, 255));
        lblLogo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblLogo.setText(".");

        jLabel21.setBackground(new java.awt.Color(255, 255, 255));
        jLabel21.setFont(new java.awt.Font("UVN Bai Hoc", 1, 18)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(0, 204, 204));
        jLabel21.setText("Nhà hàng Coupon Dish Việt Nam");

        jLabel22.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel22.setText("Điện thoại:");

        jLabel23.setFont(new java.awt.Font("Times New Roman", 1, 12)); // NOI18N
        jLabel23.setText("Địa chỉ:");

        jLabel24.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel24.setText("0901199300");

        jLabel26.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel26.setText("227 Nguyễn Văn Cừ");

        jLabel27.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        jLabel27.setText("Quận 01 - Tp. Hồ Chí Minh");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel21)
                .addGap(39, 39, 39))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel22)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel24))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel26))
                    .addComponent(jLabel27))
                .addGap(0, 8, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel22)
                            .addComponent(jLabel24))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel23)
                            .addComponent(jLabel26))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel27))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblLogo, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
        );

        jLabel25.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(0, 153, 153));
        jLabel25.setText("Tìm kiếm:");

        jTextField1.setFont(new java.awt.Font("SansSerif", 1, 14)); // NOI18N

        panelThucdon.setBackground(new java.awt.Color(255, 255, 255));
        panelThucdon.setPreferredSize(new java.awt.Dimension(310, 450));

        javax.swing.GroupLayout panelThucdonLayout = new javax.swing.GroupLayout(panelThucdon);
        panelThucdon.setLayout(panelThucdonLayout);
        panelThucdonLayout.setHorizontalGroup(
            panelThucdonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelThucdonLayout.setVerticalGroup(
            panelThucdonLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 427, Short.MAX_VALUE)
        );

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Dollar.png"))); // NOI18N
        jButton1.setText("Thanh toán");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(pntrangthai, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane2)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(btnGoimon)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1)
                        .addGap(54, 54, 54)
                        .addComponent(jButton3)
                        .addGap(18, 18, 18)
                        .addComponent(jButton4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel25)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelThucdon, javax.swing.GroupLayout.DEFAULT_SIZE, 363, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel25)
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(panelThucdon, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                .addGap(18, 18, 18)
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(pntrangthai, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnGoimon)
                                .addComponent(jButton3)
                                .addComponent(jButton4)
                                .addComponent(jButton1)))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cbbNhanVienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbNhanVienActionPerformed

    }//GEN-LAST:event_cbbNhanVienActionPerformed

    private void tbChitietGoiMonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbChitietGoiMonMouseClicked


    }//GEN-LAST:event_tbChitietGoiMonMouseClicked

    private void btnGoimonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGoimonActionPerformed
        frmGoiMon frm = new frmGoiMon(maNVBA);
        frm.setVisible(true);
        frmGoiMon.lbBanAn.setText(idBanan_Tenban);
        frmGoiMon.cbbNhanVien.setSelectedItem(nhanVien);
    }//GEN-LAST:event_btnGoimonActionPerformed
    public void checkNull() {
        if (txtPhiDV.getText().equals("")) {
            txtPhiDV.setText("0");
        }

        if (!checkNumber(txtPhiDV.getText())) {
            JOptionPane.showMessageDialog(null, "Số tiền phí dịch vụ phải lớn hơn không hoặc để trống", "Thông báo ", JOptionPane.INFORMATION_MESSAGE);
            return;
        }

    }

    public boolean checkNumber(String val) {
        try {
            if (Double.parseDouble(val) > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            return false;
        }

    }
    private void txtPhiDVKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPhiDVKeyPressed
        txtThanhToan.setText(formatNumberDecimal(f_tinhtien()));
    }//GEN-LAST:event_txtPhiDVKeyPressed

    private void txtPhiDVActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPhiDVActionPerformed
        spinnerGiamGiaPT.requestFocus();
    }//GEN-LAST:event_txtPhiDVActionPerformed

    private void spinnerGiamGiaPTStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerGiamGiaPTStateChanged

        txtThanhToan.setText(formatNumberDecimal(f_tinhtien()));
    }//GEN-LAST:event_spinnerGiamGiaPTStateChanged

    private void spinnerVATStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_spinnerVATStateChanged
        txtThanhToan.setText(formatNumberDecimal(f_tinhtien()));
    }//GEN-LAST:event_spinnerVATStateChanged

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int res = JOptionPane.showConfirmDialog(getContentPane(), "Bạn chắc chắn muốn thanh toán ?", "Chú ý....", JOptionPane.YES_NO_OPTION);
        if (res == JOptionPane.YES_OPTION) {
            daoGM.updateThanhtoanGoimon(maNVBA);
            HoadonDAO hoadonDAO = new HoadonDAO();
            Nhanvienbanan nv = new Nhanvienbanan();
            nv.setId(maNVBA);
            String giamgia = txtGiamGia.getText().replace(",", "");
            Hoadon hd = new Hoadon(1, nv, f_tinhtien(), new Date(), Double.parseDouble(giamgia));
            hoadonDAO.themBanAn(hd);
            loadListTable();
            loadChitietGoimon(maNVBA);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    public double f_tinhtien() {
        double phiDV = 0;
        try {
            if (checkNumber(txtPhiDV.getText())) {
                phiDV = Double.parseDouble(txtPhiDV.getText());
            }
        } catch (Exception e) {
            phiDV = 0;
        }
        double temp = TongTien + phiDV;
        Double giamgia = Double.parseDouble(spinnerGiamGiaPT.getValue().toString()) * temp / 100;
        Double vat = Double.parseDouble(spinnerVAT.getValue().toString()) * temp / 100;
        txtGiamGia.setText(formatNumberDecimal(giamgia) + "");
        txtVAT.setText(formatNumberDecimal(vat) + "");
        return (temp - giamgia + vat);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frmSoDNhanvienBanan frm = new frmSoDNhanvienBanan();
                frm.setVisible(true);
                instance = frm;
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnGoimon;
    private javax.swing.JComboBox<String> cbbNhanVien;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField8;
    private javax.swing.JLabel lblLogo;
    private javax.swing.JPanel panelBA;
    private javax.swing.JPanel panelThucdon;
    private javax.swing.JPanel pntrangthai;
    private javax.swing.JSpinner spinnerGiamGiaPT;
    private javax.swing.JSpinner spinnerVAT;
    private javax.swing.JTable tbChitietGoiMon;
    private javax.swing.JTextField txtGiamGia;
    private javax.swing.JTextField txtKhachDua;
    private javax.swing.JTextField txtPhiDV;
    private javax.swing.JTextField txtSoluong;
    private javax.swing.JTextField txtTenBan;
    private javax.swing.JTextField txtThanhToan;
    private javax.swing.JTextField txtThoigian;
    private javax.swing.JTextField txtTongTien;
    private javax.swing.JTextField txtVAT;
    // End of variables declaration//GEN-END:variables
}
