/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import CustomJTable.CustomMonAnRenderer;
import dao.MonanDAO;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.util.List;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import pojo.Monan;
import java.util.Date;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.JLabel;

public final class frmMenus extends javax.swing.JFrame {

    private JList<Monan> listMA;
    private JList<Monan> listMenu;
    DefaultListModel<Monan> modelMonan = new DefaultListModel<>();
    DefaultListModel<Monan> modelMenu = new DefaultListModel<>();
    static frmMenus frame;
    MonanDAO dao = new MonanDAO();
    private List<Monan> list;

    public frmMenus() {
        initComponents();

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                createMainPanel();
            }
        });

        tickTock();

    }

    private void createMainPanel() {
        pnMonAn.setLayout(new BorderLayout());

        pnMonAn.add(new JScrollPane(listMA = createListMonans()),
                BorderLayout.CENTER);

        pnMenu.setLayout(new BorderLayout());

        pnMenu.add(new JScrollPane(listMenu = createListMenu()),
                BorderLayout.CENTER);
    }

    private JList<Monan> createListMenu() {
        list = dao.layDanhSachMonanSudung(1);
        for (Monan monan : list) {
            modelMenu.addElement(monan);
        }
        JList<Monan> listMn = new JList<>(modelMenu);
        listMn.setCellRenderer(new CustomMonAnRenderer());
        return listMn;
    }

    private JList<Monan> createListMonans() {
        list = dao.layDanhSachMonanSudung(2);
        for (Monan monan : list) {
            modelMonan.addElement(monan);
        }

        JList<Monan> listMA = new JList<>(modelMonan);

        listMA.setCellRenderer(new CustomMonAnRenderer());
        return listMA;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pnMonAn = new javax.swing.JPanel();
        pnMenu = new javax.swing.JPanel();
        btnPush = new javax.swing.JButton();
        btnPop = new javax.swing.JButton();
        btnPushAll = new javax.swing.JButton();
        btnPopAll = new javax.swing.JButton();
        txtTenMonSeacrh = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        labelTime = new javax.swing.JLabel();
        lblThoigian = new javax.swing.JLabel();
        btnSeach = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));

        pnMonAn.setBackground(new java.awt.Color(255, 255, 255));
        pnMonAn.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnMonAn.setMaximumSize(new java.awt.Dimension(300, 430));
        pnMonAn.setMinimumSize(new java.awt.Dimension(300, 430));
        pnMonAn.setPreferredSize(new java.awt.Dimension(333, 450));

        javax.swing.GroupLayout pnMonAnLayout = new javax.swing.GroupLayout(pnMonAn);
        pnMonAn.setLayout(pnMonAnLayout);
        pnMonAnLayout.setHorizontalGroup(
            pnMonAnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 331, Short.MAX_VALUE)
        );
        pnMonAnLayout.setVerticalGroup(
            pnMonAnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        pnMenu.setBackground(new java.awt.Color(255, 255, 255));
        pnMenu.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        pnMenu.setMaximumSize(new java.awt.Dimension(300, 430));
        pnMenu.setPreferredSize(new java.awt.Dimension(333, 430));

        javax.swing.GroupLayout pnMenuLayout = new javax.swing.GroupLayout(pnMenu);
        pnMenu.setLayout(pnMenuLayout);
        pnMenuLayout.setHorizontalGroup(
            pnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 331, Short.MAX_VALUE)
        );
        pnMenuLayout.setVerticalGroup(
            pnMenuLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 448, Short.MAX_VALUE)
        );

        btnPush.setBackground(new java.awt.Color(255, 255, 255));
        btnPush.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPush.setForeground(new java.awt.Color(102, 102, 0));
        btnPush.setText(">");
        btnPush.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPushActionPerformed(evt);
            }
        });

        btnPop.setBackground(new java.awt.Color(255, 255, 255));
        btnPop.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btnPop.setForeground(new java.awt.Color(102, 102, 0));
        btnPop.setText("<");
        btnPop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPopActionPerformed(evt);
            }
        });

        btnPushAll.setBackground(new java.awt.Color(255, 255, 255));
        btnPushAll.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        btnPushAll.setForeground(new java.awt.Color(102, 102, 0));
        btnPushAll.setText(">>");
        btnPushAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPushAllActionPerformed(evt);
            }
        });

        btnPopAll.setBackground(new java.awt.Color(255, 255, 255));
        btnPopAll.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        btnPopAll.setForeground(new java.awt.Color(102, 102, 0));
        btnPopAll.setText("<<");
        btnPopAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPopAllActionPerformed(evt);
            }
        });

        txtTenMonSeacrh.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N

        jLabel1.setBackground(new java.awt.Color(0, 153, 51));
        jLabel1.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 153, 102));
        jLabel1.setText("DANH SÁCH MÓN ĂN");

        btnSave.setBackground(new java.awt.Color(255, 255, 255));
        btnSave.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        btnSave.setForeground(new java.awt.Color(51, 51, 51));
        btnSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Save.png"))); // NOI18N
        btnSave.setText("Lưu");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 153, 102));
        jLabel3.setText("DANH SÁCH THỰC ĐƠN");

        labelTime.setBackground(new java.awt.Color(255, 255, 255));
        labelTime.setFont(new java.awt.Font("SansSerif", 2, 11)); // NOI18N

        lblThoigian.setBackground(new java.awt.Color(255, 255, 255));
        lblThoigian.setFont(new java.awt.Font("SansSerif", 0, 11)); // NOI18N
        lblThoigian.setForeground(new java.awt.Color(102, 102, 0));
        lblThoigian.setText("Loading.....");

        btnSeach.setBackground(new java.awt.Color(255, 255, 255));
        btnSeach.setFont(new java.awt.Font("SansSerif", 1, 13)); // NOI18N
        btnSeach.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Search.png"))); // NOI18N
        btnSeach.setText("Tìm kiếm");
        btnSeach.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSeachActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addGap(82, 82, 82))
            .addGroup(layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnSeach)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtTenMonSeacrh, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(pnMonAn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnPushAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPopAll, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPush, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnPop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnMenu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSave)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(labelTime, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblThoigian, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(146, 146, 146)
                        .addComponent(btnPushAll)
                        .addGap(18, 18, 18)
                        .addComponent(btnPush)
                        .addGap(18, 18, 18)
                        .addComponent(btnPop)
                        .addGap(18, 18, 18)
                        .addComponent(btnPopAll))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblThoigian)
                        .addGap(3, 3, 3)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pnMonAn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(pnMenu, javax.swing.GroupLayout.DEFAULT_SIZE, 450, Short.MAX_VALUE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSave)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTenMonSeacrh, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSeach))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(labelTime))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPushActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPushActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if (listMA.getSelectedValue() != null) {

                    DefaultListModel mdMenu = (DefaultListModel) listMenu.getModel();
                    DefaultListModel mdMonAn = (DefaultListModel) listMA.getModel();
                    if (mdMenu == null) {
                        mdMenu = new DefaultListModel();
                        listMenu.setModel(mdMenu);
                    }
                    mdMenu.addElement(listMA.getSelectedValue());
                    mdMonAn.removeElement(listMA.getSelectedValue());
                } else {
                    JOptionPane.showMessageDialog(frame, "Vui lòng chọn một món ăn", "Thất bại !!!",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });

    }//GEN-LAST:event_btnPushActionPerformed

    private void btnPopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPopActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                if (listMenu.getSelectedValue() != null) {
                    DefaultListModel mdMenu = (DefaultListModel) listMenu.getModel();
                    DefaultListModel mdMonAn = (DefaultListModel) listMA.getModel();
                    if (mdMonAn == null) {
                        mdMonAn = new DefaultListModel();
                        listMA.setModel(mdMonAn);
                    }
                    mdMonAn.addElement(listMenu.getSelectedValue());
                    mdMenu.removeElement(listMenu.getSelectedValue());
                } else {
                    JOptionPane.showMessageDialog(frame, "Vui lòng chọn một món ăn", "Thất bại !!!",
                            JOptionPane.WARNING_MESSAGE);
                }
            }
        });
    }//GEN-LAST:event_btnPopActionPerformed

    private void btnPushAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPushAllActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DefaultListModel mdMenu = (DefaultListModel) listMenu.getModel();
                DefaultListModel mdMonAn = (DefaultListModel) listMA.getModel();
                if (mdMenu == null) {
                    mdMenu = new DefaultListModel();
                    listMenu.setModel(mdMenu);
                }
                for (int i = 0; i < mdMonAn.getSize(); i++) {
                    mdMenu.addElement(mdMonAn.elementAt(i));

                }
                mdMonAn.removeAllElements();
            }
        });
    }//GEN-LAST:event_btnPushAllActionPerformed

    private void btnPopAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPopAllActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                DefaultListModel mdMenu = (DefaultListModel) listMenu.getModel();
                DefaultListModel mdMonAn = (DefaultListModel) listMA.getModel();
                if (mdMenu == null) {
                    mdMenu = new DefaultListModel();
                    listMenu.setModel(mdMenu);
                }
                for (int i = 0; i < mdMenu.getSize(); i++) {
                    mdMonAn.addElement(mdMenu.elementAt(i));
                }
                mdMenu.removeAllElements();
            }
        });

    }//GEN-LAST:event_btnPopAllActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        DefaultListModel<Monan> model = (DefaultListModel<Monan>) listMenu.getModel();
        if (model != null) {
            String mamon = "";
            int size = model.getSize();
            for (int i = 0; i < size; i++) {
                Monan monan = (Monan) model.getElementAt(i);
                mamon += monan.getId() + ",";
            }
            if (size > 0) {
                dao.suaTrangthaisudung(mamon.substring(0, mamon.length() - 1));
            } else {
                dao.suaTrangthaisudung("");
            }
    }//GEN-LAST:event_btnSaveActionPerformed
    }
    private void btnSeachActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSeachActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                modelMonan = new DefaultListModel<>();

                if (!txtTenMonSeacrh.getText().trim().equals("")) {
                    String tenmon = txtTenMonSeacrh.getText();
                    List<Monan> list = dao.timKiemMonanTheoSudung(tenmon.toUpperCase());
                    if (list != null) {
                        for (Monan monan : list) {
                            modelMonan.addElement(monan);
                        }
                        listMA.setModel(modelMonan);
                    } else {
                        JOptionPane.showMessageDialog(frame, "Nhập tên món ăn cần tìm..", "Cảnh báo!!!",
                                JOptionPane.WARNING_MESSAGE);
                    }
                } else {
                    modelMonan = new DefaultListModel<>();
                    for (Monan monan : dao.layDanhSachMonanSudung(2)) {
                        modelMonan.addElement(monan);
                    }
                    listMA.setModel(modelMonan);
                }
            }
        });
    }//GEN-LAST:event_btnSeachActionPerformed

    public void tickTock() {
        Timer timer = new Timer(1000, new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lblThoigian.setText(DateFormat.getDateTimeInstance().format(new Date()));
            }
        });
        timer.setRepeats(true);
        timer.setCoalesce(true);
        timer.setInitialDelay(0);
        timer.start();
        labelTime.setHorizontalAlignment(JLabel.CENTER);
        labelTime.setFont(UIManager.getFont("Label.font").deriveFont(Font.BOLD, 12f));

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                frame = new frmMenus();
                frame.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPop;
    private javax.swing.JButton btnPopAll;
    private javax.swing.JButton btnPush;
    private javax.swing.JButton btnPushAll;
    private javax.swing.JButton btnSave;
    private javax.swing.JButton btnSeach;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel labelTime;
    private javax.swing.JLabel lblThoigian;
    private javax.swing.JPanel pnMenu;
    private javax.swing.JPanel pnMonAn;
    private javax.swing.JTextField txtTenMonSeacrh;
    // End of variables declaration//GEN-END:variables
}
