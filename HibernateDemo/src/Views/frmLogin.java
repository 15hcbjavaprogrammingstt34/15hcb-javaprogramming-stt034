/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import dao.LoginDAO;
import pojo.Login;
import javax.swing.JOptionPane;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

public class frmLogin extends javax.swing.JFrame {

    LoginDAO dao = new LoginDAO();

    public frmLogin() {
        initComponents();
    }

    void login() {
        try {
            String username = new String(txtusername.getText());
            String password = new String(txtpassword.getPassword());
            Login tk = dao.layThongTinLogin(username, password);
            if (tk != null) {
                new MainActivity().setVisible(true);
                dispose();
            } else {
                JOptionPane.showMessageDialog(null, "Tên đăng nhập hoặc mật khẩu không đúng. \n Vui lòng liên hệ quản trị viên.");
                txtpassword.setText(null);
                txtusername.grabFocus();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtusername = new javax.swing.JTextField();
        btnLogin = new javax.swing.JButton();
        txtpassword = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();
        lblHeading = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Login");

        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jLabel2.setForeground(new java.awt.Color(102, 255, 255));
        jLabel2.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel2.setText("Password :");

        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 90, 20));

        jLabel3.setFont(new java.awt.Font("Times New Roman", 1, 13)); // NOI18N
        jLabel3.setText("Username :");
        //jLabel3.setIcon(new ImageIcon("Asserts/images/username.PNG"));
        jLabel3.setForeground(new java.awt.Color(102, 255, 255));
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 90, 20));

        txtusername.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtusernameKeyPressed(evt);
            }
        });
        getContentPane().add(txtusername, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 50, 160, -1));

        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        getContentPane().add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 130, 80, -1));

        txtpassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpasswordKeyPressed(evt);
            }
        });
        getContentPane().add(txtpassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 90, 160, -1));

        jButton1.setText("Cancel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 130, 80, -1));

        lblHeading.setFont(new java.awt.Font("Time New Roman", 0, 18)); // NOI18N
        BufferedImage img = null;
        try {
            File file = new File("Asserts/images/headerlogin.png");
            if (file.exists()) {
                img = ImageIO.read(file);
                java.awt.Image dimg = img.getScaledInstance(65, 65, java.awt.Image.SCALE_SMOOTH);
                lblHeading.setIcon(new javax.swing.ImageIcon(dimg)); // NOI18N
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        getContentPane().add(lblHeading, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 65, 65));

        jLabel1.setIcon(new javax.swing.ImageIcon(("Asserts/images/IMGLOGIN.jpg"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 400, 220));

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width - 408) / 2, (screenSize.height - 250) / 2, 408, 250);
    }// </editor-fold>                       

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {
        login();
    }

    private void txtusernameKeyPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == 10) {
            txtpassword.grabFocus();
        }
    }

    private void txtpasswordKeyPressed(java.awt.event.KeyEvent evt) {
        if (evt.getKeyCode() == 10) {
            login();
        }
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
        dispose();
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmLogin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify                    
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel lblHeading;
    private javax.swing.JPasswordField txtpassword;
    private javax.swing.JTextField txtusername;

    // End of variables declaration         
    public class MainMenu extends javax.swing.JFrame {

        public MainMenu() {
            initComponents();
        }

        @SuppressWarnings("unchecked")
        // <editor-fold defaultstate="collapsed" desc="Generated Code">                         
        private void initComponents() {

            jLabel2 = new javax.swing.JLabel();
            jLabel3 = new javax.swing.JLabel();
            jButton1 = new javax.swing.JButton();
            jButton2 = new javax.swing.JButton();
            jButton3 = new javax.swing.JButton();
            jLabel1 = new javax.swing.JLabel();
            jMenuBar1 = new javax.swing.JMenuBar();
            jMenu1 = new javax.swing.JMenu();
            jMenuItem1 = new javax.swing.JMenuItem();
            jMenuItem2 = new javax.swing.JMenuItem();

            setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
            setTitle("Book Keepr Edition 1.0");
            setResizable(false);
            addWindowListener(new java.awt.event.WindowAdapter() {
                public void windowClosing(java.awt.event.WindowEvent evt) {
                    formWindowClosing(evt);
                }
            });
            getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

            jLabel2.setFont(new java.awt.Font("Time New Roman", 0, 36)); // NOI18N
            jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            jLabel2.setText("Book Manager");
            getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 240, 230, 50));

            jLabel3.setFont(new java.awt.Font("Time New Roman", 0, 14)); // NOI18N
            jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            jLabel3.setText("Version 1.0");
            getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 280, 100, 30));

            jButton1.setIcon(new javax.swing.ImageIcon(("Asserts/images/abc.jpg"))); // NOI18N
            jButton1.setBorderPainted(false);
            jButton1.setContentAreaFilled(false);
            jButton1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            jButton1.setFocusPainted(false);
            jButton1.setRolloverIcon(new javax.swing.ImageIcon(("Asserts/images/abc.jpg"))); // NOI18N
            jButton1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton1ActionPerformed(evt);
                }
            });
            getContentPane().add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 420, 260, 90));

            jButton2.setIcon(new javax.swing.ImageIcon("Asserts/images/abc.jpg")); // NOI18N
            jButton2.setBorderPainted(false);
            jButton2.setContentAreaFilled(false);
            jButton2.setFocusPainted(false);
            jButton2.setRolloverIcon(new javax.swing.ImageIcon("Asserts/images/abc.jpg")); // NOI18N
            jButton2.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton2ActionPerformed(evt);
                }
            });
            getContentPane().add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 420, 230, 90));

            jButton3.setIcon(new javax.swing.ImageIcon("Asserts/images/abc.jpg")); // NOI18N
            jButton3.setBorderPainted(false);
            jButton3.setContentAreaFilled(false);
            jButton3.setFocusPainted(false);
            jButton3.setRolloverIcon(new javax.swing.ImageIcon("Asserts/images/abc.jpg")); // NOI18N
            jButton3.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jButton3ActionPerformed(evt);
                }
            });
            getContentPane().add(jButton3, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 410, -1, 110));

            jLabel1.setIcon(new javax.swing.ImageIcon("Asserts/images/abc.jpg")); // NOI18N
            getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 810, 550));

            jMenu1.setText("File");

            jMenuItem1.setText("Logout");
            jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItem1ActionPerformed(evt);
                }
            });
            jMenu1.add(jMenuItem1);

            jMenuItem2.setText("Exit");
            jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
                public void actionPerformed(java.awt.event.ActionEvent evt) {
                    jMenuItem2ActionPerformed(evt);
                }
            });
            jMenu1.add(jMenuItem2);

            jMenuBar1.add(jMenu1);

            setJMenuBar(jMenuBar1);

            java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
            setBounds((screenSize.width - 809) / 2, (screenSize.height - 581) / 2, 809, 581);
        }// </editor-fold>                       

        private void formWindowClosing(java.awt.event.WindowEvent evt) {
            int i = JOptionPane.showConfirmDialog(null, "Bạn muốn thoát khỏi hệ thống?", "Thoát", JOptionPane.YES_NO_OPTION);
            if (i == JOptionPane.YES_OPTION) {
                dispose();
            } else {
            }
        }

        private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {
            new frmLogin().setVisible(true);
            dispose();
        }

        private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {
            dispose();
        }

        private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {
            //new Author().setVisible(true);
        }

        private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {
            //new Editor().setVisible(true);
        }

        private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {
            // new Book().setVisible(true);
        }

        public void main(String args[]) {
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    new MainMenu().setVisible(true);
                }
            });
        }

        // Variables declaration - do not modify                    
        private javax.swing.JButton jButton1;
        private javax.swing.JButton jButton2;
        private javax.swing.JButton jButton3;
        private javax.swing.JLabel jLabel1;
        private javax.swing.JLabel jLabel2;
        private javax.swing.JLabel jLabel3;
        private javax.swing.JMenu jMenu1;
        private javax.swing.JMenuBar jMenuBar1;
        private javax.swing.JMenuItem jMenuItem1;
        private javax.swing.JMenuItem jMenuItem2;
        // End of variables declaration                  
    }
}
