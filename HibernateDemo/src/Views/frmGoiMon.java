/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

import java.util.List;

import java.awt.Dimension;
import java.awt.Font;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;

import CustomJTable.CustomCellRenderer;
import dao.MonanDAO;
import dao.NhanvienDAO;
import dao.GoimonDAO;
import pojo.Nhanvienbanan;
import pojo.Monan;
import pojo.Nhanvien;
import pojo.Goimon;
import dao.NhanvienbananDAO;
import dao.TrangthaimonanDAO;
import java.awt.HeadlessException;
import java.util.Calendar;
import java.util.Date;
import pojo.Trangthaimonan;

/**
 *
 * @author VietHoang
 */
public class frmGoiMon extends javax.swing.JFrame {

    MonanDAO daoMonan = new MonanDAO();
    NhanvienDAO daoNV = new NhanvienDAO();
    GoimonDAO daoGM = new GoimonDAO();
    List<Monan> listMonan;
    List<Nhanvien> listNV;
    List<Goimon> listGM;
    int idNVBA;
    private String tenBan = "";
    NhanvienbananDAO daoNvBA = new NhanvienbananDAO();
    TrangthaimonanDAO daoTrangthaimonan = new TrangthaimonanDAO();

    public void setTenBan(String tenBan) {
        this.tenBan = tenBan;
    }

    /**
     * Creates new form frmGoiMon
     */
    public frmGoiMon(int idNVBA) {
        this.idNVBA = idNVBA;
        listMonan = daoMonan.layDanhSachMonan();
        initComponents();
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                loadJCombobox();
            }
        });

        loadChitietGoimon(idNVBA);//idNVBA
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                loadMonan();
            }
        });
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    public frmGoiMon() {
        initComponents();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                loadJCombobox();
            }
        });
        loadChitietGoimon(idNVBA);//idNVBA
        listMonan = daoMonan.layDanhSachMonan();
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                loadMonan();
            }
        });
    }

    public String formatNumber(int num) {
        NumberFormat formatter = new DecimalFormat("000");
        return formatter.format(num);
    }

    public void loadJCombobox() {

        listNV = daoNV.layDanhSachNhanvien();

        Nhanvien nvPos = null;
        DefaultComboBoxModel cbModelNV = new DefaultComboBoxModel();
        for (Nhanvien vt : listNV) {
            if (vt.getManv() == frmSoDNhanvienBanan.idNV) {
                nvPos = vt;
            }
            cbModelNV.addElement(vt);
        }
        cbbNhanVien.setModel(cbModelNV);
        if (nvPos != null) {
            cbbNhanVien.setSelectedItem(nvPos);
        }
        cbbNhanVien.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
        buttonGroup1.add(rdNuocUong);
        buttonGroup1.add(rdThucAn);
        cbbMoAn.setPreferredSize(new Dimension(150, 23));
        cbbMoAn.setEditable(true);
    }

    public void loadMonan() {
        DefaultComboBoxModel cbModelMA = new DefaultComboBoxModel();
        for (Monan vt : listMonan) {
            if (rdThucAn.isSelected()) {
                if (vt.getLoaimonan().getId() == 1) {
                    cbModelMA.addElement(vt);
                }
            } else if (vt.getLoaimonan().getId() == 2) {
                cbModelMA.addElement(vt);
            }

        }

        cbbMoAn.setModel(cbModelMA);
        cbbMoAn.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
        Monan obj = (Monan) cbbMoAn.getSelectedItem();
        txtDonGia.setText(obj.getDongia() + "");
    }

    public void loadChitietGoimon(int idnvba) {
//        try {
        listGM = daoGM.layDanhSachTheoIdNvba(idnvba);
//        } catch (Exception e) {
//        }
        if (listGM != null) {
            int i = 1;
            DefaultTableModel model = new DefaultTableModel(null, new Object[]{"STT", "Tên món", "Số lượng", "Đơn giá", "Tình trạng", "ID", "IDMONAN"});
            for (Goimon g : listGM) {
                model.addRow(new Object[]{formatNumber(i), g.getMonan().getTenmon(), g.getSoluong(), g.getMonan().getDongia(), g.getTrangthaimonan().getTentt(), g.getId(), g.getMonan().getId()});
                i++;
            }
            tbChitietGoiMon.setModel(model);
            tbChitietGoiMon.getColumnModel().getColumn(5).setMinWidth(0);
            tbChitietGoiMon.getColumnModel().getColumn(5).setMaxWidth(0);
            tbChitietGoiMon.getColumnModel().getColumn(5).setWidth(0);
            tbChitietGoiMon.getColumnModel().getColumn(6).setMinWidth(0);
            tbChitietGoiMon.getColumnModel().getColumn(6).setMaxWidth(0);
            tbChitietGoiMon.getColumnModel().getColumn(6).setWidth(0);

            tbChitietGoiMon.getColumnModel().getColumn(0).setMinWidth(45);
            tbChitietGoiMon.getColumnModel().getColumn(0).setMaxWidth(45);
            tbChitietGoiMon.getColumnModel().getColumn(0).setWidth(45);
            tbChitietGoiMon.setDefaultRenderer(Object.class, new CustomCellRenderer());
            lbBanAn.setText(tenBan);
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        cbbNhanVien = new javax.swing.JComboBox<String>();
        cbbMoAn = new javax.swing.JComboBox<String>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbChitietGoiMon = new javax.swing.JTable();
        rdThucAn = new javax.swing.JRadioButton();
        rdNuocUong = new javax.swing.JRadioButton();
        txtDonGia = new javax.swing.JTextField();
        spinnerSL = new javax.swing.JSpinner();
        btnThem = new javax.swing.JButton();
        btnXoa = new javax.swing.JButton();
        btnSua = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setMaximumSize(new java.awt.Dimension(654, 368));
        setMinimumSize(new java.awt.Dimension(654, 368));

        cbbNhanVien.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        cbbNhanVien.setForeground(new java.awt.Color(0, 102, 102));
        cbbNhanVien.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        cbbNhanVien.setMaximumSize(new java.awt.Dimension(180, 23));
        cbbNhanVien.setMinimumSize(new java.awt.Dimension(180, 23));
        cbbNhanVien.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbNhanVienActionPerformed(evt);
            }
        });

        cbbMoAn.setFont(new java.awt.Font("Times New Roman", 0, 14)); // NOI18N
        cbbMoAn.setForeground(new java.awt.Color(153, 0, 0));
        cbbMoAn.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbbMoAn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbMoAnActionPerformed(evt);
            }
        });

        tbChitietGoiMon.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tbChitietGoiMon.setMaximumSize(new java.awt.Dimension(291, 410));
        tbChitietGoiMon.setMinimumSize(new java.awt.Dimension(291, 410));
        tbChitietGoiMon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbChitietGoiMonMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbChitietGoiMon);

        rdThucAn.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        rdThucAn.setForeground(new java.awt.Color(0, 0, 255));
        rdThucAn.setSelected(true);
        rdThucAn.setText("Đồ ăn");
        rdThucAn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdThucAnActionPerformed(evt);
            }
        });

        rdNuocUong.setFont(new java.awt.Font("Serif", 0, 14)); // NOI18N
        rdNuocUong.setForeground(new java.awt.Color(0, 0, 255));
        rdNuocUong.setText("Thức uống");
        rdNuocUong.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rdNuocUongActionPerformed(evt);
            }
        });

        txtDonGia.setEditable(false);
        txtDonGia.setBackground(new java.awt.Color(255, 255, 255));
        txtDonGia.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtDonGia.setForeground(new java.awt.Color(102, 0, 0));

        spinnerSL.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        spinnerSL.setModel(new javax.swing.SpinnerNumberModel(1, 1, 99, 1));

        btnThem.setBackground(new java.awt.Color(153, 153, 255));
        btnThem.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        btnThem.setForeground(new java.awt.Color(102, 0, 51));
        btnThem.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Add.png"))); // NOI18N
        btnThem.setText("Thêm");
        btnThem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnThemActionPerformed(evt);
            }
        });

        btnXoa.setBackground(new java.awt.Color(153, 153, 255));
        btnXoa.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        btnXoa.setForeground(new java.awt.Color(102, 0, 51));
        btnXoa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Delete.png"))); // NOI18N
        btnXoa.setText("Xóa");
        btnXoa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnXoaActionPerformed(evt);
            }
        });

        btnSua.setBackground(new java.awt.Color(153, 153, 255));
        btnSua.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        btnSua.setForeground(new java.awt.Color(102, 0, 51));
        btnSua.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Update.png"))); // NOI18N
        btnSua.setText("Sửa");
        btnSua.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSuaActionPerformed(evt);
            }
        });

        btnExit.setBackground(new java.awt.Color(153, 153, 255));
        btnExit.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        btnExit.setForeground(new java.awt.Color(102, 0, 51));
        btnExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Close.png"))); // NOI18N
        btnExit.setText("Thoát");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Loại");

        jLabel2.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Chi tiết");

        jLabel3.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Đơn giá");

        jLabel4.setFont(new java.awt.Font("Serif", 1, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Số lượng");

        jLabel5.setFont(new java.awt.Font("Serif", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(0, 102, 102));
        jLabel5.setText("Nhân viên:");

        jLabel6.setBackground(new java.awt.Color(0, 51, 51));
        jLabel6.setFont(new java.awt.Font("SansSerif", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 102));
        jLabel6.setText("DANH SÁCH MÓN ĂN");

        lbBanAn.setBackground(new java.awt.Color(255, 255, 255));
        lbBanAn.setFont(new java.awt.Font("SansSerif", 1, 12)); // NOI18N
        lbBanAn.setForeground(new java.awt.Color(0, 102, 102));
        lbBanAn.setText("jLabel7");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(6, 6, 6))
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(spinnerSL, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(81, 81, 81))
                                    .addComponent(btnXoa, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addComponent(rdThucAn)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(rdNuocUong))))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtDonGia, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addComponent(btnThem)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnSua, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnExit))
                    .addComponent(lbBanAn)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(cbbMoAn, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbbNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel6)
                .addGap(203, 203, 203))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(39, 39, 39)
                        .addComponent(lbBanAn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rdThucAn)
                            .addComponent(rdNuocUong)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(cbbMoAn, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtDonGia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(spinnerSL, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnThem)
                            .addComponent(btnXoa))
                        .addGap(28, 28, 28)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSua)
                            .addComponent(btnExit)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbbNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void rdThucAnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdThucAnActionPerformed
        loadMonan();
    }//GEN-LAST:event_rdThucAnActionPerformed

    private void rdNuocUongActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rdNuocUongActionPerformed
        loadMonan();
    }//GEN-LAST:event_rdNuocUongActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        dispose();
        frmSoDNhanvienBanan getInstance = frmSoDNhanvienBanan.getInstance();
        getInstance.loadListTable();
    }//GEN-LAST:event_btnExitActionPerformed

    private void cbbMoAnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbMoAnActionPerformed
        Monan obj = (Monan) cbbMoAn.getSelectedItem();
        txtDonGia.setText(obj.getDongia() + "");

    }//GEN-LAST:event_cbbMoAnActionPerformed

    private void cbbNhanVienActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbNhanVienActionPerformed
        Nhanvien obj = (Nhanvien) cbbNhanVien.getSelectedItem();

        if (obj != null) {
            try {
                Calendar cal = Calendar.getInstance().getInstance();
                cal.getTime().toString();
                if (daoNvBA.upNhanVienBanAn(obj.getManv(), 2, idNVBA, cal.getTime().toString())) {
                    JOptionPane.showMessageDialog(null, "Chọn nhân viên phụ trách thành công!", "Thành công !!!",
                            JOptionPane.INFORMATION_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(null, "Không thể cập nhật.Liên hệ quản trị viên !!!", "Thất bại !!!",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Không thể cập nhật.Liên hệ quản trị viên !!!", "Thất bại !!!",
                        JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
        }


    }//GEN-LAST:event_cbbNhanVienActionPerformed

    private void btnThemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnThemActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    Goimon g;
                    Monan monan = (Monan) cbbMoAn.getSelectedItem();
                    Nhanvienbanan nvBA = NhanvienbananDAO.layThongTinNhanvienbanan(idNVBA);
                    Trangthaimonan ttMonan = TrangthaimonanDAO.layThongTinTrangthaimonan(1);
                    g = new Goimon(1, monan, nvBA, ttMonan, Integer.valueOf(spinnerSL.getValue().toString()), 1);//
                    if (daoGM.themGoimon(g)) {
                        JOptionPane.showMessageDialog(null, "Thêm thành công!", "Thành công !!!",
                                JOptionPane.INFORMATION_MESSAGE);
                        loadChitietGoimon(idNVBA);//idNVBA
                    } else {
                        JOptionPane.showMessageDialog(null, "Thêm thất bại.Liên hệ quản trị viên !!!", "Thất bại !!!",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (NumberFormatException | HeadlessException e) {
                    JOptionPane.showMessageDialog(null, "Thêm thất bại.Liên hệ quản trị viên !!!", "Thất bại !!!",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });

    }//GEN-LAST:event_btnThemActionPerformed

    private void btnXoaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnXoaActionPerformed
        try {
            int ID = 0;
            try {
                int selectedRow = tbChitietGoiMon.getSelectedRow();
                selectedRow = tbChitietGoiMon.convertRowIndexToModel(selectedRow);
                ID = Integer.valueOf(tbChitietGoiMon.getModel().getValueAt(selectedRow, 5).toString());

            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Vui lòng chọn dòng cần xóa !!!", "Thông báo",
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            int res = JOptionPane.showConfirmDialog(getContentPane(), "Bạn chắc chắn muốn xóa bàn này ?", "Chú ý....", JOptionPane.YES_NO_OPTION);
            if (res == JOptionPane.YES_OPTION) {

                if (daoGM.xoaGoimon(ID)) {
                    JOptionPane.showMessageDialog(null, "Xóa thành công!!!", "Thành công !!!",
                            JOptionPane.INFORMATION_MESSAGE);
                    loadChitietGoimon(idNVBA);//idNVBA
                } else {
                    JOptionPane.showMessageDialog(null, "Thất bại !!!", "Xóa thất bại",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Vui lòng chọn bàn cần xóa !!!", "Thất bại !!!",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_btnXoaActionPerformed

    private void btnSuaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSuaActionPerformed
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    int ID = 0;
                    try {
                        int selectedRow = tbChitietGoiMon.getSelectedRow();
                        selectedRow = tbChitietGoiMon.convertRowIndexToModel(selectedRow);
                        ID = Integer.valueOf(tbChitietGoiMon.getModel().getValueAt(selectedRow, 5).toString());

                    } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, "Vui lòng chọn dòng cần sửa !!!", "Thông báo",
                                JOptionPane.INFORMATION_MESSAGE);
                        return;
                    }
                    Goimon g;
                    Monan monan = (Monan) cbbMoAn.getSelectedItem();
                    Nhanvienbanan nvBA = NhanvienbananDAO.layThongTinNhanvienbanan(idNVBA);
                    Trangthaimonan ttMonan = TrangthaimonanDAO.layThongTinTrangthaimonan(1);
                    g = new Goimon(ID, monan, nvBA, ttMonan, Integer.valueOf(spinnerSL.getValue().toString()), 1);//

                    if (daoGM.capNhatThongTinGoimon(g)) {
                        JOptionPane.showMessageDialog(null, "Sửa thành công!", "Thông báo !!!",
                                JOptionPane.INFORMATION_MESSAGE);
                        loadChitietGoimon(idNVBA);//idNVBA
                    } else {
                        JOptionPane.showMessageDialog(null, "Sửa thất bại.Liên hệ quản trị viên !!!", "Thông báo !!!",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, "Thêm thất bại.Liên hệ quản trị viên !!!", "Thất bại !!!",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
        });
    }//GEN-LAST:event_btnSuaActionPerformed

    private void tbChitietGoiMonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbChitietGoiMonMouseClicked

        int selectedRow = tbChitietGoiMon.getSelectedRow();
        selectedRow = tbChitietGoiMon.convertRowIndexToModel(selectedRow);
        txtDonGia.setText(tbChitietGoiMon.getModel().getValueAt(selectedRow, 3).toString());
        int idMonan = (Integer) tbChitietGoiMon.getModel().getValueAt(selectedRow, 6);
        Monan monAn = daoMonan.layThongTinMonan(idMonan);
        cbbMoAn.setSelectedItem(monAn);
        spinnerSL.setValue(new Integer(tbChitietGoiMon.getModel().getValueAt(selectedRow, 2).toString()));


    }//GEN-LAST:event_tbChitietGoiMonMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmGoiMon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmGoiMon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmGoiMon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmGoiMon.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frmGoiMon frm = new frmGoiMon();
                frm.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnSua;
    private javax.swing.JButton btnThem;
    private javax.swing.JButton btnXoa;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> cbbMoAn;
    public static javax.swing.JComboBox<String> cbbNhanVien;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    public static final javax.swing.JLabel lbBanAn = new javax.swing.JLabel();
    private javax.swing.JRadioButton rdNuocUong;
    private javax.swing.JRadioButton rdThucAn;
    private javax.swing.JSpinner spinnerSL;
    private javax.swing.JTable tbChitietGoiMon;
    private javax.swing.JTextField txtDonGia;
    // End of variables declaration//GEN-END:variables
}
