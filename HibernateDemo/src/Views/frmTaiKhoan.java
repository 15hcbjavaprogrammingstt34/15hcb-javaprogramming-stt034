/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;

//import Controller.NhanVienDAO;
//import Controller.LoginDAO;
//import Controller.VitrilamviecDAO;
//import Model.NhanVien;
//import Model.Login;
//import Model.Vitrilamviec;
import dao.LoginDAO;
import dao.NhanvienDAO;
import dao.ViTriLamViecDAO;
import java.awt.Color;
import java.awt.Font;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import pojo.Login;
import pojo.Nhanvien;
import pojo.Vitrilamviec;

/**
 *
 * @author VietHoang
 */
public class frmTaiKhoan extends javax.swing.JFrame {

    LoginDAO dao = new LoginDAO();
    ViTriLamViecDAO daoVitri = new ViTriLamViecDAO();
    NhanvienDAO daoNV = new NhanvienDAO();
    List<Login> listTK;
    private Thread thLoadTable, thLoadCombobox;

    /**
     * Creates new form frmLogin
     */
    public frmTaiKhoan() {
        initComponents();
         listTK = dao.layDanhSachTaikhoan();
                loadJTable(listTK);
        thLoadCombobox = new Thread(rLoadCombobox);
        thLoadCombobox.start();

    }

    public void loadTable() {
        thLoadTable = new Thread(rLoadTable);
        thLoadTable.start();
    }

    Runnable rLoadTable = new Runnable() {
        public void run() {
            try {
                listTK = dao.layDanhSachTaikhoan();
                loadJTable(listTK);
            } catch (Exception x) {
            }
        }
    };
    Runnable rLoadCombobox = new Runnable() {
        public void run() {
            try {
                loadJCombobox();
                eventJTableClick();
            } catch (Exception x) {
            }
        }
    };

    public static Color hex2Rgb(String colorStr) {
        return new Color(
                Integer.valueOf(colorStr.substring(1, 3), 16),
                Integer.valueOf(colorStr.substring(3, 5), 16),
                Integer.valueOf(colorStr.substring(5, 7), 16));
    }

    public void eventJTableClick() {
        JTableHeader header = jtbTaiKhoan.getTableHeader();
        header.setBackground(hex2Rgb("#4FABFF"));
        header.setForeground(Color.white);
        jtbTaiKhoan.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                try {
                    TableModel model = jtbTaiKhoan.getModel();
                    //"STT", "Username", "Password", "Họ Tên", "Tên vị trí", "ID", "MaNV", "MaVT"
                    int selectedRow = jtbTaiKhoan.getSelectedRow();
                    selectedRow = jtbTaiKhoan.convertRowIndexToModel(selectedRow);
                    String username = model.getValueAt(selectedRow, 1).toString();
                    String passwd = model.getValueAt(selectedRow, 2).toString();
                    String hoten = model.getValueAt(selectedRow, 3).toString();
                    String tenVT = model.getValueAt(selectedRow, 4).toString();
                    int ID = Integer.valueOf(jtbTaiKhoan.getModel().getValueAt(selectedRow, 5).toString());
                    int maNV = Integer.valueOf(jtbTaiKhoan.getModel().getValueAt(selectedRow, 6).toString());
                    int maVT = Integer.valueOf(jtbTaiKhoan.getModel().getValueAt(selectedRow, 7).toString());

                    txtID.setText(ID + "");
                    txtUserName.setText(username);
                    txtPassWd.setText(passwd);
                    cbbVitri.getModel().setSelectedItem(new Vitrilamviec(maVT, tenVT));
                    Nhanvien nhanVien = daoNV.layThongTinNhanvien(ID);
                    cbbNhanVien.getModel().setSelectedItem(nhanVien);
                } catch (Exception e) {
                    System.out.printf(e.toString());
                }

            }
        });
    }

    public void loadJCombobox() {
        List<Vitrilamviec> listVT = daoVitri.layDanhSachVitrilamviec();
        DefaultComboBoxModel cbModelVitri = new DefaultComboBoxModel();
        for (Vitrilamviec vt : listVT) {
            cbModelVitri.addElement(vt);
        }
        cbbVitri.setModel(cbModelVitri);
        cbbVitri.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
        List<Nhanvien> listNV = daoNV.layDanhSachNhanvien();
        DefaultComboBoxModel cbModelNV = new DefaultComboBoxModel();
        for (Nhanvien vt : listNV) {
            cbModelNV.addElement(vt);
        }
        cbbNhanVien.setModel(cbModelNV);
        cbbNhanVien.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
    }

    public void loadJTable(List<Login> list) {
        int i = 1;
        DefaultTableModel model = new DefaultTableModel(null, new Object[]{"STT", "Username", "Password", "Họ Tên", "Tên vị trí", "ID", "MaNV", "MaVT"});
        for (Login tk : list) {
            model.addRow(new Object[]{i, tk.getUsername(), tk.getPasswd(), tk.getNhanvien().getHoten(), tk.getVitrilamviec().getTenvitri(), tk.getId(), tk.getNhanvien().getManv(), tk.getVitrilamviec().getId()});
            i++;
        }
        jtbTaiKhoan.setModel(model);
        jtbTaiKhoan.getColumnModel().getColumn(0).setMinWidth(35);
        jtbTaiKhoan.getColumnModel().getColumn(0).setMaxWidth(35);
        jtbTaiKhoan.getColumnModel().getColumn(0).setWidth(35);

        jtbTaiKhoan.getColumnModel().getColumn(5).setMinWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(5).setMaxWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(5).setWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(6).setMinWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(6).setMaxWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(6).setWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(7).setMinWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(7).setMaxWidth(0);
        jtbTaiKhoan.getColumnModel().getColumn(7).setWidth(0);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        jtbTaiKhoan.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        jtbTaiKhoan.setFont(new Font("San-Serif", Font.LAYOUT_LEFT_TO_RIGHT, 12));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jtbTaiKhoan = new javax.swing.JTable();
        cbbNhanVien = new javax.swing.JComboBox<String>();
        cbbVitri = new javax.swing.JComboBox<String>();
        txtID = new javax.swing.JTextField();
        txtUserName = new javax.swing.JTextField();
        txtPassWd = new javax.swing.JTextField();
        btnAdd = new javax.swing.JButton();
        btnDel = new javax.swing.JButton();
        btnUpdate = new javax.swing.JButton();
        btnExit = new javax.swing.JButton();
        btnSearch = new javax.swing.JButton();
        chkHoten = new javax.swing.JCheckBox();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jtbTaiKhoan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jtbTaiKhoan);

        cbbVitri.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbbVitriActionPerformed(evt);
            }
        });

        txtID.setEditable(false);
        txtID.setBackground(new java.awt.Color(255, 255, 255));

        txtUserName.setText(" ");

        txtPassWd.setText(" ");

        btnAdd.setBackground(new java.awt.Color(153, 153, 255));
        btnAdd.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(153, 0, 0));
        btnAdd.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Add.png"))); // NOI18N
        btnAdd.setText("Thêm");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnDel.setBackground(new java.awt.Color(153, 153, 255));
        btnDel.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnDel.setForeground(new java.awt.Color(204, 0, 0));
        btnDel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Delete.png"))); // NOI18N
        btnDel.setText("Xóa");
        btnDel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDelActionPerformed(evt);
            }
        });

        btnUpdate.setBackground(new java.awt.Color(153, 153, 255));
        btnUpdate.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnUpdate.setForeground(new java.awt.Color(204, 0, 0));
        btnUpdate.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Update.png"))); // NOI18N
        btnUpdate.setText("Sửa");
        btnUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateActionPerformed(evt);
            }
        });

        btnExit.setBackground(new java.awt.Color(153, 153, 255));
        btnExit.setFont(new java.awt.Font("Tahoma", 1, 10)); // NOI18N
        btnExit.setForeground(new java.awt.Color(204, 0, 0));
        btnExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Exit.png"))); // NOI18N
        btnExit.setText("Thoát");
        btnExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExitActionPerformed(evt);
            }
        });

        btnSearch.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(0, 0, 102));
        btnSearch.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icon/Search.png"))); // NOI18N
        btnSearch.setText("Tìm kiếm");

        chkHoten.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        chkHoten.setForeground(new java.awt.Color(102, 0, 102));
        chkHoten.setText("Họ tên");

        jCheckBox1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jCheckBox1.setForeground(new java.awt.Color(102, 0, 102));
        jCheckBox1.setText("Vị trí làm việc");

        jLabel1.setText("ID");

        jLabel2.setText("Username:");

        jLabel3.setText("Password:");

        jLabel4.setText("Họ tên:");

        jLabel5.setText("Vị trí:");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 1, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 102));
        jLabel6.setText("QUẢN LÝ TÀI KHOẢN");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtUserName)
                                .addGap(63, 63, 63))
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtPassWd, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbbNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(cbbVitri, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(btnUpdate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(50, 50, 50)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btnExit)
                                    .addComponent(btnDel, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 11, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(btnSearch)
                        .addGap(27, 27, 27)
                        .addComponent(chkHoten, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jCheckBox1)
                        .addGap(75, 75, 75))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(layout.createSequentialGroup()
                .addGap(260, 260, 260)
                .addComponent(jLabel6)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel6)
                        .addGap(18, 18, 18)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnSearch)
                            .addComponent(chkHoten)
                            .addComponent(jCheckBox1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(73, 73, 73)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtID, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtUserName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtPassWd, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbbNhanVien, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbbVitri, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))
                        .addGap(78, 78, 78)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnAdd)
                            .addComponent(btnDel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnUpdate)
                            .addComponent(btnExit))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cbbVitriActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbbVitriActionPerformed
        Vitrilamviec vt = (Vitrilamviec) cbbVitri.getSelectedItem();
    }//GEN-LAST:event_cbbVitriActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        String userName = txtUserName.getText();
        String passWd = txtPassWd.getText();
        Vitrilamviec vl = (Vitrilamviec) cbbVitri.getSelectedItem();
        Nhanvien nv = (Nhanvien) cbbNhanVien.getSelectedItem();
        Login tk = new Login(1, nv, vl, userName, passWd);
        try {

            if (dao.themBanAn(tk)) {
                JOptionPane.showMessageDialog(this, "Thêm tài khoản mới thành công");
                loadTable();
            } else {
                JOptionPane.showMessageDialog(this, "Thêm tài khoản mới thất bại");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnDelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDelActionPerformed
        String userName = txtUserName.getText();
        String passWd = txtPassWd.getText();
        Vitrilamviec vl = (Vitrilamviec) cbbVitri.getSelectedItem();
        Nhanvien nv = (Nhanvien) cbbNhanVien.getSelectedItem();
        int ID = Integer.valueOf(txtID.getText());

        try {

            if (dao.xoaLogin(ID)) {
                JOptionPane.showMessageDialog(null, "Xóa thành công !!!");
                loadTable();
            } else {
                JOptionPane.showMessageDialog(null, "Xóa thất bại !!!");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnDelActionPerformed

    private void btnUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateActionPerformed
        String userName = txtUserName.getText();
        String passWd = txtPassWd.getText();
        Vitrilamviec vl = (Vitrilamviec) cbbVitri.getSelectedItem();
        Nhanvien nv = (Nhanvien) cbbNhanVien.getSelectedItem();
        int ID = Integer.valueOf(txtID.getText());
        Login tk = new Login(ID, nv, vl, userName, passWd);
        try {
            if (dao.capNhatThongTinBanAn(tk)) {
                JOptionPane.showMessageDialog(this, "Sửa tài khoản mới thành công");
                loadTable();
            } else {
                JOptionPane.showMessageDialog(this, "Sửa tài khoản mới thất bại");
            }
        } catch (Exception e) {
        }
    }//GEN-LAST:event_btnUpdateActionPerformed

    private void btnExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExitActionPerformed
        setVisible(false); //you can't see me!
        dispose();
    }//GEN-LAST:event_btnExitActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        int i = JOptionPane.showConfirmDialog(null, "Bạn muốn thoát khỏi hệ thống?", "Thoát", JOptionPane.YES_NO_OPTION);
        if (i == JOptionPane.YES_OPTION) {
            dispose();
             
        } else {
        }
    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        frmLogin frm = new frmLogin();
        frm.setVisible(true);
        frm.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        frm.setLocationRelativeTo(null);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnDel;
    private javax.swing.JButton btnExit;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnUpdate;
    private javax.swing.JComboBox<String> cbbNhanVien;
    private javax.swing.JComboBox<String> cbbVitri;
    private javax.swing.JCheckBox chkHoten;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtbTaiKhoan;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtPassWd;
    private javax.swing.JTextField txtUserName;
    // End of variables declaration//GEN-END:variables
}
