package pojo;
// Generated May 2, 2016 4:13:37 PM by Hibernate Tools 4.3.1

import java.util.HashSet;
import java.util.Set;

/**
 * Vitrilamviec generated by hbm2java
 */
public class Vitrilamviec implements java.io.Serializable {

    private int id;
    private String tenvitri;
    private Set nhanviens = new HashSet(0);
    private Set logins = new HashSet(0);
    private Set nhanvientheocas = new HashSet(0);

    public Vitrilamviec() {
    }

    public String toString() {
        return this.tenvitri;
    }

    public Vitrilamviec(int id, String tenvitri) {
        this.id = id;
        this.tenvitri = tenvitri;
    }

    public Vitrilamviec(int id, String tenvitri, Set nhanviens, Set logins, Set nhanvientheocas) {
        this.id = id;
        this.tenvitri = tenvitri;
        this.nhanviens = nhanviens;
        this.logins = logins;
        this.nhanvientheocas = nhanvientheocas;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenvitri() {
        return this.tenvitri;
    }

    public void setTenvitri(String tenvitri) {
        this.tenvitri = tenvitri;
    }

    public Set getNhanviens() {
        return this.nhanviens;
    }

    public void setNhanviens(Set nhanviens) {
        this.nhanviens = nhanviens;
    }

    public Set getLogins() {
        return this.logins;
    }

    public void setLogins(Set logins) {
        this.logins = logins;
    }

    public Set getNhanvientheocas() {
        return this.nhanvientheocas;
    }

    public void setNhanvientheocas(Set nhanvientheocas) {
        this.nhanvientheocas = nhanvientheocas;
    }

}
