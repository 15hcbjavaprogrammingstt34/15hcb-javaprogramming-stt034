package pojo;
// Generated May 2, 2016 4:13:37 PM by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Bangluong generated by hbm2java
 */
public class Bangluong  implements java.io.Serializable {


     private int maluong;
     private Nhanvien nhanvien;
     private Date ngaygionhan;
     private int tongcalam;
     private Integer vangcophep;
     private Integer vangkhongphep;
     private Integer thoigiantre;
     private int tongluong;

    public Bangluong() {
    }

	
    public Bangluong(int maluong, Date ngaygionhan, int tongcalam, int tongluong) {
        this.maluong = maluong;
        this.ngaygionhan = ngaygionhan;
        this.tongcalam = tongcalam;
        this.tongluong = tongluong;
    }
    public Bangluong(int maluong, Nhanvien nhanvien, Date ngaygionhan, int tongcalam, Integer vangcophep, Integer vangkhongphep, Integer thoigiantre, int tongluong) {
       this.maluong = maluong;
       this.nhanvien = nhanvien;
       this.ngaygionhan = ngaygionhan;
       this.tongcalam = tongcalam;
       this.vangcophep = vangcophep;
       this.vangkhongphep = vangkhongphep;
       this.thoigiantre = thoigiantre;
       this.tongluong = tongluong;
    }
   
    public int getMaluong() {
        return this.maluong;
    }
    
    public void setMaluong(int maluong) {
        this.maluong = maluong;
    }
    public Nhanvien getNhanvien() {
        return this.nhanvien;
    }
    
    public void setNhanvien(Nhanvien nhanvien) {
        this.nhanvien = nhanvien;
    }
    public Date getNgaygionhan() {
        return this.ngaygionhan;
    }
    
    public void setNgaygionhan(Date ngaygionhan) {
        this.ngaygionhan = ngaygionhan;
    }
    public int getTongcalam() {
        return this.tongcalam;
    }
    
    public void setTongcalam(int tongcalam) {
        this.tongcalam = tongcalam;
    }
    public Integer getVangcophep() {
        return this.vangcophep;
    }
    
    public void setVangcophep(Integer vangcophep) {
        this.vangcophep = vangcophep;
    }
    public Integer getVangkhongphep() {
        return this.vangkhongphep;
    }
    
    public void setVangkhongphep(Integer vangkhongphep) {
        this.vangkhongphep = vangkhongphep;
    }
    public Integer getThoigiantre() {
        return this.thoigiantre;
    }
    
    public void setThoigiantre(Integer thoigiantre) {
        this.thoigiantre = thoigiantre;
    }
    public int getTongluong() {
        return this.tongluong;
    }
    
    public void setTongluong(int tongluong) {
        this.tongluong = tongluong;
    }




}


