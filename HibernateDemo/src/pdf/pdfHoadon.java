/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pdf;

import java.io.FileOutputStream;
import java.io.File;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.FontSelector;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dao.MonanDAO;
import java.text.DecimalFormat;
import pojo.Monan;

public class pdfHoadon {

    static int stt = 0;
    private static String FILE;
    public static File fontFile = new File("Asserts/font/times.TTF");

    public static void main(String[] args) {
        try {
            Image image = Image.getInstance("src/icon/Apply.png");
            image.scaleAbsolute(50, 20);
            //image.setAbsolutePosition(100f, 700f);
            stt++;
            FILE = "D:/pdfhoadon.pdf";

//          Font font1 = new Font(Font.FontFamily.HELVETICA  , 25, Font.BOLD);
//          Font font2 = new Font(Font.FontFamily.COURIER    , 18,
//          Font.ITALIC | Font.UNDERLINE);
            BaseFont unicode = BaseFont.createFont(fontFile.getAbsolutePath(), BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            FontSelector fs = new FontSelector();
            fs.addFont(new Font(unicode));

            Document document = new Document(PageSize.A4, 1f, 2f, 2f, 2f);
            PdfWriter.getInstance(document, new FileOutputStream(FILE));
            document.open();
            // Left
            Paragraph paragraph = new Paragraph("nó thì căn phải", new Font(unicode, 14f, Font.BOLD));
            paragraph.setAlignment(Element.ALIGN_RIGHT);
            document.add(paragraph);
            // Centered
            paragraph = new Paragraph("This is centered text");
            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.add(image);
            document.add(paragraph);

            //document.add(image);
            // Left
            paragraph = new Paragraph("This is left aligned text");
            paragraph.setAlignment(Element.ALIGN_LEFT);
            document.add(paragraph);
            // Left with indentation
            paragraph = new Paragraph("This is left aligned text with indentation");
            paragraph.setAlignment(Element.ALIGN_LEFT);
            paragraph.setIndentationLeft(50);
            PdfPTable table = new PdfPTable(3);

            table.setWidths(new float[]{40f, 200f, 200f});

            PdfPCell c1 = new PdfPCell(new Phrase("STT"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Ten mon"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);

            c1 = new PdfPCell(new Phrase("Don gia"));
            c1.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(c1);
            table.setHeaderRows(1);

            java.util.List<Monan> list = new MonanDAO().layDanhSachMonan();
            int i = 0;
            PdfPCell cell;
            for (Monan monan : list) {
                i++;
                cell = new PdfPCell(new Phrase(formatSTT(i)));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(monan.getTenmon(), new Font(unicode)));
                table.addCell(cell);
                table.addCell(monan.getDongia() + "");
            }
            cell = new PdfPCell(new Phrase("Tong cong"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setColspan(2);
            table.addCell(cell);
            
             table.addCell("100000000");
            
            
            
            paragraph.add(table);
            document.add(paragraph);
            document.close();
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String formatSTT(int i) {
        DecimalFormat fomat = new DecimalFormat("000");
        return fomat.format(i);
    }
}
