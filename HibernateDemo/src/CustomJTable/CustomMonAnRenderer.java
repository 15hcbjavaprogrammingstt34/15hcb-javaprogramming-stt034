/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CustomJTable;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.ListCellRenderer;
import pojo.Monan;

public class CustomMonAnRenderer extends JPanel implements ListCellRenderer<Monan> {

    private JLabel lbIcon = new JLabel();
    private JLabel lbName = new JLabel();
    private JLabel lbAuthor = new JLabel();
    JSeparator jSeparator = new JSeparator();

    public CustomMonAnRenderer() {
        setLayout(new BorderLayout(5, 5));
        JPanel panelText = new JPanel(new GridLayout(0, 1));
        panelText.add(lbName);
        panelText.add(lbAuthor);
        add(lbIcon, BorderLayout.WEST);
        add(panelText, BorderLayout.CENTER);
        add(jSeparator, BorderLayout.PAGE_END);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends Monan> list,
            Monan book, int index, boolean isSelected, boolean cellHasFocus) {
        try {
            if (book.getImg().trim().equals("")) {
                book.setImg("src\\icon\\iconMenus.png");
            }
            BufferedImage buffer = ImageIO.read(new File(book.getImg()));//book.getImg()src\\icon\\iconMenus.png
            java.awt.Image dimg = buffer.getScaledInstance(120, 80,
                    java.awt.Image.SCALE_SMOOTH);
            ImageIcon imageIcon = new ImageIcon(dimg);

            lbIcon.setIcon(imageIcon);
            lbIcon.setPreferredSize(new Dimension(120, 80));
            lbIcon.repaint();
            lbName.setText(book.getTenmon());
            lbAuthor.setText(book.getDongia() + "VNĐ");
            lbAuthor.setForeground(Color.blue);

            // set Opaque to change background color of JLabel
            lbName.setOpaque(true);
            lbAuthor.setOpaque(true);
            lbIcon.setOpaque(true);
        } catch (Exception e) {
        }
        // when select item
        if (isSelected) {
            lbName.setBackground(list.getSelectionBackground());
            lbAuthor.setBackground(list.getSelectionBackground());
            lbIcon.setBackground(list.getSelectionBackground());
            setBackground(list.getSelectionBackground());
        } else { // when don't select
            lbName.setBackground(list.getBackground());
            lbAuthor.setBackground(list.getBackground());
            lbIcon.setBackground(list.getBackground());
            setBackground(list.getBackground());
        }
        return this;
    }
}
