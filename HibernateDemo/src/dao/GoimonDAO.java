/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Goimon;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class GoimonDAO {

    public GoimonDAO() {
    }

    public List<Goimon> layDanhSachTheoIdNvba(int idNvba) {
        ArrayList<Goimon> ds = new ArrayList<>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Goimon sv join sv.nhanvienbanan nvba where sv.thanhtoan=1 and nvba.id =" + idNvba
                    + " order by sv.id ASC";

            Query query = session.createQuery(hql);

            List tmp = query.list();
            Goimon ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Goimon) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public double tinhTien(int idNvba) {
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sum(sv.soluong*ma.dongia) from Goimon sv join sv.nhanvienbanan nvba "
                    + " join sv.monan ma where sv.thanhtoan=1 and nvba.id =" + idNvba;

            Query query = session.createQuery(hql);

            List tmp = query.list();
            Object ba = null;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Object) it.next();

            }
            if(ba!= null)
                return Double.parseDouble( ba.toString());
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }

        return 0;
    }

    public List<Goimon> layDanhSachGoiMon() {
        List<Goimon> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Goimon sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public boolean themGoimon(Goimon sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean capNhatThongTinGoimon(Goimon sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean xoaGoimon(int maGoimon) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String sql = "delete from Goimon where id=" + maGoimon;
            Query query = session.createQuery(sql);
            query.executeUpdate();
            transaction.commit();

        } catch (HibernateException ex) {
            // transaction.rollback();
            System.err.println(ex);
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    public boolean updateThanhtoanGoimon(int maNVBA) {//1 chua 2 da thanh toan
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String sql = "update Goimon set thanhtoan=2 where thanhtoan=1 and manvba=" + maNVBA;
            Query query = session.createQuery(sql);
            query.executeUpdate();

            String sql2 = "update Nhanvienbanan set manv=null, trangthai=1 where id=" + maNVBA;
            Query query2 = session.createQuery(sql2);
            query2.executeUpdate();
            transaction.commit();

        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
            return false;
        } finally {
            session.close();
        }
        return true;
    }

    public static Goimon layThongTinGoimon(int maGoimon) {
        Goimon sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            sv = (Goimon) session.get(Goimon.class, maGoimon);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public List<Goimon> timKiemGoimon(String tenBan, int soluong) {
        List<Goimon> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Goimon sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Goimon ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Goimon) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
