/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Loaimonan;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class LoaimonanDAO {

    public LoaimonanDAO() {
    }

    public List<Loaimonan> layDanhSachLoaiMonAn() {
        List<Loaimonan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Loaimonan sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public boolean themLoaiMonAn(Loaimonan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean capNhatThongTinLoaiMonAn(Loaimonan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean xoaLoaimonan(int maLoaimonan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Loaimonan sv = LoaimonanDAO.layThongTinLoaimonan(maLoaimonan);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Loaimonan layThongTinLoaimonan(int maLoaimonan) {
        Loaimonan sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Loaimonan) session.get(Loaimonan.class, maLoaimonan);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public ArrayList<Loaimonan> timKiemLoaimonan(String tenBan, int soluong) {
        ArrayList<Loaimonan> ds = new ArrayList<Loaimonan>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Loaimonan sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Loaimonan ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Loaimonan) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
