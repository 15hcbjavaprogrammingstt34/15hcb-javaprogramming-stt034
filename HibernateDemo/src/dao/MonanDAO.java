/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Monan;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class MonanDAO {

    public MonanDAO() {
    }

    public List<Monan> layDanhSachMonan() {
        List<Monan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Monan sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public List<Monan> layDanhSachMonanSudung(int sudung) {
        List<Monan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Monan sv where sv.sudung=" + sudung;
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public boolean themMonan(Monan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
            return false;
        } finally {
            session.close();

        }
        return true;
    }

    public boolean capNhatThongTinMonan(Monan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean xoaMonan(int maMonan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Monan sv = MonanDAO.layThongTinMonan(maMonan);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String hql = "delete from Monan sv where sv.id=" + maMonan;
            Query query = session.createQuery(hql);
            //session.delete(sv);
            query.executeUpdate();
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Monan layThongTinMonan(int maMonan) {
        Monan sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Monan) session.get(Monan.class, maMonan);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public List<Monan> timKiemMonanTheoSudung(String tenMon) {
        List<Monan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Monan sv where sudung=2 ";
            if (!"".equals(tenMon)) {
                hql += " and upper(sv.tenmon) like '%" + tenMon + "%' ";
            }
            Query query = session.createQuery(hql);

            ds = query.list();
//        Monan ba;
//        for (Iterator it = tmp.iterator(); it.hasNext();) {
//            ds.add((Monan) it.next());
//            System.err.println(it.toString());
//        }
        } catch (HibernateException ex) {
            System.err.println(ex);

        } finally {
            session.close();
        }
        return ds;
    }

    public List<Monan> timKiemMonan(String tenMon, int donGia) {
        List<Monan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Monan sv where 1=1 ";
            if (!"".equals(tenMon)) {
                hql += " and upper(sv.tenmon) like '%" + tenMon.toUpperCase() + "%' ";
            }
            if (donGia > 0) {
                hql += " and sv.dongia=" + donGia;
            }
            Query query = session.createQuery(hql);
            ds = query.list();

        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public boolean suaTrangthaisudung(String mamon) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        String hql;
        try {
            transaction = session.beginTransaction();
            if (mamon.equals("")) {
                hql = " update  Monan sv set sv.sudung=2";
                Query query = session.createQuery(hql);
                query.executeUpdate();
                transaction.commit();
                return true;
            } else {
                hql = " update  Monan sv set sv.sudung=2";// 1.dc sudung 2.khong
                Query query = session.createQuery(hql);
                if (query.executeUpdate() > 0) {
                    hql = "Update Monan sv set sv.sudung=1 where 1=1 ";
                    if (mamon != "") {
                        hql += " and sv.id in(" + mamon + ")";
                    }
                    query = session.createQuery(hql);
                    if (query.executeUpdate() > 0) {
                        transaction.commit();
                        return true;
                    }
                }
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
            transaction.rollback();
        } finally {
            session.close();
        }
        return false;
    }
}
