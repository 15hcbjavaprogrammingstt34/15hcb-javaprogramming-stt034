/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Vitrilamviec;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class ViTriLamViecDAO {

 
    public ViTriLamViecDAO() {
    }

    public  List<Vitrilamviec> layDanhSachVitrilamviec() {
        List<Vitrilamviec> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Vitrilamviec sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public   boolean themBanAn(Vitrilamviec sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public   boolean capNhatThongTinBanAn(Vitrilamviec sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public   boolean xoaVitrilamviec(int maVitrilamviec) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Vitrilamviec sv = ViTriLamViecDAO.layThongTinVitrilamviec(maVitrilamviec);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public  static Vitrilamviec layThongTinVitrilamviec(int maVitrilamviec) {
        Vitrilamviec sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Vitrilamviec) session.get(Vitrilamviec.class, maVitrilamviec);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public   ArrayList<Vitrilamviec> timKiemVitrilamviec(String tenBan, int soluong) {
        ArrayList<Vitrilamviec> ds = new ArrayList<Vitrilamviec>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Vitrilamviec sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Vitrilamviec ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Vitrilamviec) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
