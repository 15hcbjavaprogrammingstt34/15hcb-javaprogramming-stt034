/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import pojo.Nhanvien;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import util.HibernateUtil;

/**
 *
 * @author VietHoang
 */
public class NhanvienDAO {

    public NhanvienDAO() {
    }

    public List<Nhanvien> layDanhSachNhanvien() {
        List<Nhanvien> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Nhanvien sv where sv.id <>0";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
//Log the exception
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public boolean themBanAn(Nhanvien sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean capNhatThongTinBanAn(Nhanvien sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean xoaNhanvien(int maNhanvien) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Nhanvien sv = NhanvienDAO.layThongTinNhanvien(maNhanvien);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Nhanvien layThongTinNhanvien(int maNhanvien) {
        Nhanvien sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            sv = (Nhanvien) session.get(Nhanvien.class, maNhanvien);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public ArrayList<Nhanvien> timKiemNhanvien(String tenBan, int soluong) {
        ArrayList<Nhanvien> ds = new ArrayList<Nhanvien>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Nhanvien sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Nhanvien ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Nhanvien) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
