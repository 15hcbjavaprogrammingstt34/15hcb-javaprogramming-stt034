/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Trangthaithanhtoan;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class TrangthaithanhtoanDAO {

    public TrangthaithanhtoanDAO() {
    }

    public static List<Trangthaithanhtoan> layDanhSachBA() {
        List<Trangthaithanhtoan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Trangthaithanhtoan sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public static boolean themBanAn(Trangthaithanhtoan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static boolean capNhatThongTinBanAn(Trangthaithanhtoan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static boolean xoaTrangthaithanhtoan(int maTrangthaithanhtoan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Trangthaithanhtoan sv = TrangthaithanhtoanDAO.layThongTinTrangthaithanhtoan(maTrangthaithanhtoan);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Trangthaithanhtoan layThongTinTrangthaithanhtoan(int maTrangthaithanhtoan) {
        Trangthaithanhtoan sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Trangthaithanhtoan) session.get(Trangthaithanhtoan.class, maTrangthaithanhtoan);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public static ArrayList<Trangthaithanhtoan> timKiemTrangthaithanhtoan(String tenBan, int soluong) {
        ArrayList<Trangthaithanhtoan> ds = new ArrayList<Trangthaithanhtoan>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Trangthaithanhtoan sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Trangthaithanhtoan ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Trangthaithanhtoan) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

}
