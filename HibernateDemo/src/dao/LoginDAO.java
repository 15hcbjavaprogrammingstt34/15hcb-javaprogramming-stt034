/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Login;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class LoginDAO {

    public LoginDAO() {
    }

    public static List<Login> layDanhSachTaikhoan() {
        List<Login> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Login sv where sv.id<>0 order by sv.id";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public void initConnect() {
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        session.beginTransaction();
    }

    public boolean themBanAn(Login sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean capNhatThongTinBanAn(Login sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean xoaLogin(int maLogin) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Login sv = LoginDAO.layThongTinLogin(maLogin);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Login layThongTinLogin(int maLogin) {
        Login sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Login) session.get(Login.class, maLogin);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public static Login layThongTinLogin(String username, String passwd) {
        Login tk = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            String sql = " select a from Login a where  a.username='"
                    + username + "' and a.passwd='" + passwd + "'";
            Query query = session.createQuery(sql);
            List tmp = query.list();
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                tk = (Login) it.next();
                break;
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return tk;
    }

    public ArrayList<Login> timKiemLogin(String tenBan, int soluong) {
        ArrayList<Login> ds = new ArrayList<Login>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Login sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Login ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Login) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
