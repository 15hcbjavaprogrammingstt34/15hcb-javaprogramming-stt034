/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Trangthaibanan;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class TrangthaibananDAO {

    public TrangthaibananDAO() {
    }

    public static List<Trangthaibanan> layDanhSachBA() {
        List<Trangthaibanan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Trangthaibanan sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public static boolean themBanAn(Trangthaibanan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static boolean capNhatThongTinBanAn(Trangthaibanan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static boolean xoaTrangthaibanan(int maTrangthaibanan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Trangthaibanan sv = TrangthaibananDAO.layThongTinTrangthaibanan(maTrangthaibanan);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Trangthaibanan layThongTinTrangthaibanan(int maTrangthaibanan) {
        Trangthaibanan sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Trangthaibanan) session.get(Trangthaibanan.class, maTrangthaibanan);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public static ArrayList<Trangthaibanan> timKiemTrangthaibanan(String tenBan, int soluong) {
        ArrayList<Trangthaibanan> ds = new ArrayList<Trangthaibanan>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Trangthaibanan sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Trangthaibanan ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Trangthaibanan) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
