/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Trangthaimonan;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class TrangthaimonanDAO {

    public TrangthaimonanDAO() {
    }

    public static List<Trangthaimonan> layDanhSachBA() {
        List<Trangthaimonan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Trangthaimonan sv";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public static boolean themBanAn(Trangthaimonan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static boolean capNhatThongTinBanAn(Trangthaimonan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static boolean xoaTrangthaimonan(int maTrangthaimonan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Trangthaimonan sv = TrangthaimonanDAO.layThongTinTrangthaimonan(maTrangthaimonan);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Trangthaimonan layThongTinTrangthaimonan(int maTrangthaimonan) {
        Trangthaimonan sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Trangthaimonan) session.get(Trangthaimonan.class, maTrangthaimonan);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public static ArrayList<Trangthaimonan> timKiemTrangthaimonan(String tenBan, int soluong) {
        ArrayList<Trangthaimonan> ds = new ArrayList<Trangthaimonan>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Trangthaimonan sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Trangthaimonan ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Trangthaimonan) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
