/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojo.Nhanvienbanan;
import util.HibernateUtil;

/**
 *
 * @author PC_Viethoang
 */
public class NhanvienbananDAO {

    public NhanvienbananDAO() {
    }

    public boolean upNhanVienBanAn(int manv, int matrangthai, int idNVBA, String date) {
        int val = -1;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            String sql = " UPDATE Nhanvienbanan SET manv=" + manv + ",trangthai=" + matrangthai + ", ngay='" + date + "' WHERE id=" + idNVBA;
            Query query = session.createQuery(sql);
            val = query.executeUpdate();
            transaction.commit();
            return true;
        } catch (Exception ex) {
            transaction.rollback();
        } finally {
            session.close();
        }
        return false;
    }

    public List<Nhanvienbanan> layDanhSachNhanvienbanan() {
        List<Nhanvienbanan> ds = null;
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = "select sv from Nhanvienbanan sv order by sv.id ASC";
            Query query = session.createQuery(hql);
            ds = query.list();
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }

    public boolean themNhanvienbanan(Nhanvienbanan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean capNhatThongTinNhanvienbanan(Nhanvienbanan sv) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public boolean xoaNhanvienbanan(int maNhanvienbanan) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Nhanvienbanan sv = NhanvienbananDAO.layThongTinNhanvienbanan(maNhanvienbanan);
        if (sv == null) {
            return false;
        }
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(sv);
            transaction.commit();
        } catch (HibernateException ex) {
            transaction.rollback();
            System.err.println(ex);
        } finally {
            session.close();
        }
        return true;
    }

    public static Nhanvienbanan layThongTinNhanvienbanan(int maNhanvienbanan) {
        Nhanvienbanan sv = null;
        Session session = HibernateUtil.getSessionFactory().openSession();

        try {
            sv = (Nhanvienbanan) session.get(Nhanvienbanan.class, maNhanvienbanan);
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return sv;
    }

    public ArrayList<Nhanvienbanan> timKiemNhanvienbanan(String tenBan, int soluong) {
        ArrayList<Nhanvienbanan> ds = new ArrayList<Nhanvienbanan>();
        Session session = HibernateUtil.getSessionFactory()
                .openSession();
        try {
            String hql = " select sv from Nhanvienbanan sv where 1=1 ";
            if (tenBan != "") {
                hql += " and sv.tenban like '%" + tenBan + "%' ";
            }
            if (soluong > 0) {
                hql += " and sv.soluong=" + soluong;
            }
            Query query = session.createQuery(hql);
            List tmp = query.list();
            Nhanvienbanan ba;
            for (Iterator it = tmp.iterator(); it.hasNext();) {
                ba = (Nhanvienbanan) it.next();
                ds.add(ba);
            }
        } catch (HibernateException ex) {
            System.err.println(ex);
        } finally {
            session.close();
        }
        return ds;
    }
}
